$(function(){
	var viewportmeta = document.querySelector && document.querySelector('meta[name="viewport"]'),
	ua = navigator.userAgent,

	gestureStart = function () {viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6";},

	scaleFix = function () {
		if (viewportmeta && /iPhone|iPad/.test(ua) && !/Opera Mini/.test(ua)) {
			viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0";
			document.addEventListener("gesturestart", gestureStart, false);
		}
	};
	
	scaleFix();
});
var ua=navigator.userAgent.toLocaleLowerCase(),
 regV = /ipod|ipad|iphone/gi,
 result = ua.match(regV),
 userScale="";
if(!result){
 userScale=",user-scalable=0"
}
document.write('<meta name="viewport" content="width=device-width,initial-scale=1.0'+userScale+'">')

// ЕСЛИ ПРОЕКТ РЕСПОНСИВ ТО ВСЕ ЧТО ВЫШЕ НУЖНО РАССКОМЕНТИРОВАТЬ. СКРИПТ ВЫШЕ ПРЕДНАЗНАЧЕН ДЛЯ КОРРЕКТНОГО ОТОБРАЖЕНИЯ ВЕРСТКИ ПРИ СМЕНЕ ОРИЕНТАЦИИ НА ДЕВАЙСАХ



// INCLUDE FUNCTION
// ниже пример подключения табов. То есть создаем переменную с селектором на который нужно выполнить инициализацию, потом условием проверяем есть ли такой селектор, если есть то скрипт подключит файл с помощью функции INCLUDE. Это важно так как в наших проектах может быть подключено по 20 библиотек это оочеь плохо так как проект становится тяжелым и переполненным мусором


var owl          = $(".owl-carousel"),
    owl2		     = $(".owl_2_item"),
  	tabsBox   	 = $("#parentHorizontalTab"),
  	countdown 	 = $(".clock"),
  	popup 	  	 = $("[data-popup]"),
    swiperh      = $(".slider_v2"),
    swiper2      = $(".swiper-container2"),
  	swiper 	  	 = $(".swiper-container3"),
  	fancyBox  	 = $(".fancybox"),
  	fullCalendar = $("#calendar"),
    weekCalendar = $("#week_calendar")
  	timeline	   = $("#timeline"),
    scrollpane   = $(".scroll-pane"),
    scrollpane2  = $(".scroll-pane2"),
    calendar     = $("#datepicker"),
    audio        = $(".audio_box"),
    oiplayer     = $(".oiplayer"),
    tooltipster  = $(".tooltip");

// icluding

if(owl.length || owl2.length ){
	include("js/owl.carousel.js");
}

if(tabsBox.length){
	include("js/easyResponsiveTabs.js");
}

if(countdown.length){
	include("js/jquery.countdown.min.js");
}

if(popup.length){
	include('js/jquery.arcticmodal.js');
}

if(swiper.length || swiper2.length || swiperh.length){
	include("js/swiper.js");
}

if(fancyBox.length){
	include("js/jquery.fancybox.js");
}

if(fullCalendar.length){
	include("js/moment.min.js");
	include("js/fullcalendar.js");
  include("js/tooltipster.bundle.js");
  include("js/jquery.qtip.min.js");
}

if(weekCalendar.length){
  include("js/moment.min.js");
  include("js/fullcalendar.js");
}

if(timeline.length){
	include("js/jquery.timeline.js");
}

if(scrollpane.length){
  include("js/jquery.jscrollpane.min.js");
  include("js/jquery.mousewheel.js");
}

if(scrollpane2.length){
  include("js/jquery.jscrollpane.min.js");
  include("js/jquery.mousewheel.js");
}

if(calendar.length){
  include("js/jquery-ui.js");
}

if(audio.length){
  include("js/audio.min.js");
}

if(oiplayer.length){
  include("js/jquery.oiplayer.js");
}

function include(url){ 
  document.write('<script src="'+ url + '"></script>'); 
}




$(document).ready(function(){

	// responsive NAV
		$(".resp_menu, .close_resp_menu").on('click ontouchstart', function(e){
	  		var body = $('body');
	  		
	  		body.toggleClass('nav_overlay');
	    })

	// DROPDOWN MENU
		$(".dropdown").on('click ontouchstart', function(e){
	  		var body = $('body');
	  		
	  		body.toggleClass('dropdown_overlay');
	    })

	// Search input
		$(".search_btn").on('click ontouchstart', function(e){
	  		var body = $('body');
	  		
	  		body.toggleClass('search_overlay');
	    })

  // LIST Calendar
    $(".listMonth, .notListMonth").on('click ontouchstart', function(e){
        var body = $('body');
        
        body.toggleClass('list_calendar');
      })


	/* Toggle between adding and removing the "active" and "show" classes when the user clicks on one of the "Section" buttons. The "active" class is used to add a background color to the current button when its belonging panel is open. The "show" class is used to open the specific accordion panel */
		var acc = document.getElementsByClassName("accordion");
		var i;

		for (i = 0; i < acc.length; i++) {
		    acc[i].onclick = function(){
		        this.classList.toggle("active");
		        this.nextElementSibling.classList.toggle("show");
		    }
		}

	/* ------------------------------------------------
	FANCYBOX START
	------------------------------------------------ */

		if (fancyBox.length){
			fancyBox.fancybox({
				autoSize: false,
		   		maxWidth: 970,
		   		maxHeight: 642,
		   		helpers : { 
					title : { type : 'inside' }
				}, 
				beforeShow : function() {
					var numb = $('.fancybox-inner');

					// this.title = (this.title ? '' + this.title + '' : '') + ' <span class="num">' + (this.index + 1) + ' of ' + this.group.length + ' </span>';

					numb.append(' <span class="num">' + (this.index + 1) + ' / ' + this.group.length + ' </span>');
				} 
		   		// afterLoad : function() {
	      //           this.title = '' + (this.index + 1) + ' / ' + this.group.length + (this.title ? ' - ' + this.title : '');
	      //       }
			});
		}

	/* ------------------------------------------------
	FANCYBOX END
	------------------------------------------------ */


	/* ------------------------------------------------
	POPUP START
	------------------------------------------------ */

		if(popup.length){
			popup.on('click',function(){
			    var modal = $(this).data("popup");
			    $(modal).arcticmodal({
			    		afterOpen: function(){
			    			swiper.swiper({
								nextButton: '.swiper-button-next',
						        prevButton: '.swiper-button-prev',
						        pagination: '.swiper-pagination',
						        paginationType: 'fraction'
							   })
              }
          })
      })
    };
 

	/* ------------------------------------------------
	POPUP END
	------------------------------------------------ */


	/* ------------------------------------------------
	SWIPER START
	------------------------------------------------ */

		if(swiper.length){
			swiper.swiper({
				nextButton: '.swiper-button-next',
        prevButton: '.swiper-button-prev',
        pagination: '.swiper-pagination',
        paginationType: 'fraction'
			})
		};

    if(swiperh.length){

       var swiperH = new Swiper('.swiper-container-h', {
        pagination: '.swiper-pagination-h',
        paginationClickable: true,
          nextButton: '.swiper-button-next2',
          prevButton: '.swiper-button-prev2'
      });
      var swiperV = new Swiper('.swiper-container-v', {
          pagination: '.swiper-pagination-v',
          paginationClickable: true,
          direction: 'vertical',
          nextButton: '.swiper-button-next',
          prevButton: '.swiper-button-prev'
      });
    };

    // if(swiper2.length){
    //   swiper2.swiper({
    //     nextButton: '.swiper-button-next',
    //         prevButton: '.swiper-button-prev',
    //         pagination: '.swiper-pagination',
    //         paginationType: 'fraction'
    //   })
    // };

	/* ------------------------------------------------
	SWIPER END
	------------------------------------------------ */


	/* ------------------------------------------------
	CAROUSEL START
	------------------------------------------------ */

		if(owl.length){

			owl.owlCarousel({
				items : 1,
			    singleItem : true,
			    navigation: true
			});

			$(".testimonials_owl").owlCarousel({
				pagination:true
			});

      owl2.owlCarousel({
        items : 2,
        pagination:true
      });
			 
		}

    if(owl2.length){

      owl2.owlCarousel({
        items : 2,
        pagination: true,
        navigation: false,
        itemsDesktop: [1199, 2],
        itemsTablet: [768, 1],
        itemsMobile: [320, 1]
      });
       
    }


	/* ------------------------------------------------
	CAROUSEL END
	------------------------------------------------ */



	/* ------------------------------------------------
	MASKEDINPUT START
	------------------------------------------------ */

		// if (owl.length){

		// 	owl.owlCarousel({
		// 	    loop:true,
		// 	    margin:10,
		// 	    items: 1,
		// 	    navigation: true
		// 	})
		// }
	/* ------------------------------------------------
	MASKEDINPUT END
	------------------------------------------------ */



	/* ------------------------------------------------
	COUNTDOWN START
	------------------------------------------------ */

		if(countdown.length){

			 countdown.countdown('2017/01/01', function(event) {
				  var $this = $(this).html(event.strftime(''
				    + '<div>%D <span>days</span></div> '
				    + '<div>%H <span>hours</span></div> '
				    + '<div>%M <span>minutes</span></div> '
				    + '<div>%S <span>secundes</span></div>'));
			});
		}
			
	/* ------------------------------------------------
	COUNTDOWN END
	------------------------------------------------ */


	/* ------------------------------------------------
	TABS START
	------------------------------------------------ */

		if(tabsBox.length){
		    //Horizontal Tab
			    $('#parentHorizontalTab').easyResponsiveTabs({
			        type: 'default', //Types: default, vertical, accordion
			        width: 'auto', //auto or any width like 600px
			        fit: true, // 100% fit in a container
			        tabidentify: 'hor_1', // The tab groups identifier
			        activate: function(event) { // Callback function if tab is switched
			            var $tab = $(this);
			            var $info = $('#nested-tabInfo');
			            var $name = $('span', $info);
			            $name.text($tab.text());
			            $info.show();
			        }
			    });
			// Child Tab
			    $('#ChildVerticalTab_1').easyResponsiveTabs({
			        type: 'vertical',
			        width: 'auto',
			        fit: true,
			        tabidentify: 'ver_1', // The tab groups identifier
			        activetab_bg: '#fff', // background color for active tabs in this group
			        inactive_bg: '#F5F5F5', // background color for inactive tabs in this group
			        active_border_color: '#c1c1c1', // border color for active tabs heads in this group
			        active_content_border_color: '#5AB1D0' // border color for active tabs contect in this group so that it matches the tab head border
			    });
			//Vertical Tab
			    $('#parentVerticalTab').easyResponsiveTabs({
			        type: 'vertical', //Types: default, vertical, accordion
			        width: 'auto', //auto or any width like 600px
			        fit: true, // 100% fit in a container
			        closed: 'accordion', // Start closed if in accordion view
			        tabidentify: 'hor_1', // The tab groups identifier
			        activate: function(event) { // Callback function if tab is switched
			            var $tab = $(this);
			            var $info = $('#nested-tabInfo2');
			            var $name = $('span', $info);
			            $name.text($tab.text());
			            $info.show();
			        }
			    }); 
  		}; 

	/* ------------------------------------------------
	TABS END
	------------------------------------------------ */


	/* ------------------------------------------------
	FULLCALENDAR START
	------------------------------------------------ */

		if(fullCalendar.length){
			$('#calendar').fullCalendar({
        header: {
          left: 'prev,next',
          center: 'title',
          right: 'month'  
        },
        defaultDate: '2016-11-12',
        navLinks: true, // can click day/week names to navigate views
        businessHours: true, // display business hours
        editable: true,
        events: [
          {
            description: '<div class="fc_tip_content"> '+
              "<h5 class='title5'>Conference Europe 2016 “I believe in Jesus Christ”</h5>"+
              "<p class='green'><span class='bold'>25 September</span> 16:00 Hrs - 20:00 Hrs</p>"+
              "<div class='img_border_box'> <img src='images/gallery_img1.jpg'/> </div>" +
              "<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin iaculis sed libero ac commodo. Morbi erosmana lobortis vitae posuere quis, magna lobortis vitae posuere quis, convallis quis lacus...</p>" +
            '</div>',
            title: 'Conference Europe 2016 “I believe in Jesus Christ”',
            start: '2016-11-15',
            allDay: false
          }, 
          {
            title: 'Birthday Party',
            start: '2016-11-11',
            allDay: false
          }
        ],
        eventRender: function(event, element) {
            element.qtip({
                my: 'top center',
                content: event.description
            });
        }
      });


      // $(window).resize(function() {
      //   var res_calendar = document.getElementsByClassName(".big_calendar");

      //   if ($(window).width() <= '768'){
      //     res_calendar.addClass("scroll-pane");
      //     $(".scroll-pane").jScrollPane();
      //   }

      // });

		}

    if(weekCalendar.length){

      $('#week_calendar').fullCalendar({
        header: {
          left: 'prev,next',
          center: 'title',
          right: false
        },
        defaultView: 'basicWeek',
        defaultDate: '2016-11-20',
        navLinks: false, // can click day/week names to navigate views
        businessHours: true, // display business hours
        editable: true,
        events: [
          {
            title: 'Night of Pray & Worship',
            start: '2016-09-03'
          },
          {
            title: 'Conference Europe 2016 “I believe in Jesus Christ”',
            start: '2016-09-05',
            constraint: 'availableForMeeting', // defined below
            color: '#257e4a'
          },
          {
            title: 'Conference “Christianity - it is much more than just going to church”',
            start: '2016-11-21',
          },
          {
            title: 'Conference Europe 2016 “I believe in Jesus Christ”',
            start: '2016-09-26'
          },
          {
            title: 'Conference 2016 “3 Strategies for Training New Team Members”',
            start: '2016-09-26'
          },

          // areas where "Meeting" must be dropped
          
          {
            id: 'availableForMeeting',
            start: '2016-11-13T10:00:00',
            end: '2016-11-13T16:00:00',
            rendering: 'background'
          },

          // red areas where no events can be dropped
          {
            start: '2016-11-24',
            end: '2016-11-28',
            overlap: false,
            rendering: 'background',
            color: '#ff9f89'
          },
          {
            start: '2016-11-06',
            end: '2016-11-08',
            overlap: false,
            rendering: 'background',
            color: '#ff9f89'
          }
        ]
      });
    }
			
	/* ------------------------------------------------
	FULLCALENDAR END
	------------------------------------------------ */


	/* ------------------------------------------------
	TIMELINE START
	------------------------------------------------ */

		if(timeline.length){

			timeline.timelinr({
				arrowKeys: 'true'
			})
		}
			
	/* ------------------------------------------------
	TIMELINE END
	------------------------------------------------ */

  /* ------------------------------------------------
  SCROLLPANE START
  ------------------------------------------------ */

    if(scrollpane.length){

      scrollpane.jScrollPane();

    }

    function windowSize(){

      if ($(window).width() <= '768'){
          scrollpane2.jScrollPane();
      }

    }

    $(window).load(windowSize);
      
  /* ------------------------------------------------
  SCROLLPANE END
  ------------------------------------------------ */


  /* ------------------------------------------------
  CALENDAR START
  ------------------------------------------------ */

    if(calendar.length){

      calendar.datepicker();

    }
      
  /* ------------------------------------------------
  CALENDAR END
  ------------------------------------------------ */

  /* ------------------------------------------------
  AUDIO START
  ------------------------------------------------ */

    if(audio.length){

      var a = audiojs;
      a.events.ready(function() {
        var a1 = a.createAll();
      });

    }
      
  /* ------------------------------------------------
  AUDIO END
  ------------------------------------------------ */

  /* ------------------------------------------------
  OIplayer START
  ------------------------------------------------ */

    if(oiplayer.length){

      oiplayer.oiplayer({
        server : 'http://www.openimages.eu', /* msie (or java) has issues with just a dir */
        jar : '/oiplayer/plugins/cortado-ovt-stripped-0.6.0.jar',
        flash : '/oiplayer/plugins/flowplayer-3.2.7.swf',
        controls : 'top'
      });

    }
      
  /* ------------------------------------------------
  OIplayer END
  ------------------------------------------------ */


})


$(document).ready(function () {

	
});